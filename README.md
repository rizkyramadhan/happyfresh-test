# happyfresh-bmi
## Run app on Machine
1. clone this repo to your local
```
git clone  git@gitlab.com:rizkyramadhan/happyfresh-bmi.git
```
2. install dependency of application
```
npm install
```
3. run application
```
npm start
```
4. access app
```
http://localhost:7777/bmicalculator
```

## Run app on Container
1. build image
```
docker build -t bmicalculator:latest .
```
2. run container from built image
```
 docker run -dit --name=bmi-calculator -p 7777:7777 bmicalculator:latest
```
3. access app
```
http://localhost:7777/bmicalculator
```
Created by Rizky Ramadhan